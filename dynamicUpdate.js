/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var ibcontent = document.getElementById("ibcontent");
var body = document.querySelector("body"); // Actually the same as ibcontent.
var _autoScrollEnabled = false;

var beforeMathjaxElementParsed = function() {
  // Duplicates convbrowser._updateAutoScrollEnabled
  _autoScrollEnabled = body.scrollHeight <= body.scrollTop + body.clientHeight + 10;
};

var afterMathjaxElementParsed = function() {
  // Use scrollToElement so as not to confuse convbrowser.
  if (_autoScrollEnabled)
    scrollToElement(endOfConvElt);
};

ibcontent.handleNewNode = function(aEvent) {
  var target = aEvent.originalTarget;
  if (!(target instanceof HTMLElement))
 	  return;

  var spans = target.getElementsByClassName("ib-msg-txt");
  if (!spans.length)
    return;

  Array.forEach(spans, function(elt) {
    MathJax.Hub.Queue(beforeMathjaxElementParsed,
                      ["Typeset", MathJax.Hub, elt],
                      afterMathjaxElementParsed);
  });
};

var endOfConvElt = document.createElement("P");
endOfConvElt.id = "mathjax-addon-endofconv";
ibcontent.appendChild(endOfConvElt);
ibcontent.addEventListener("DOMNodeInserted", ibcontent.handleNewNode);
