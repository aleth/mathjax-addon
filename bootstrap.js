/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource:///modules/imServices.jsm");
Cu.import("resource:///modules/imWindows.jsm");

const kPrefBranch = "extensions.mathjax.";
var prefs = Services.prefs.getBranch(kPrefBranch);

function getConfiguration(aShouldScroll) {
  let config =
    "MathJax.Hub.Config({" +
      // Based on TeX-AMS-MML_HTMLorMML.js, without supporting MML input.
      "config: [\"MMLorHTML.js\"]," +
      "jax: [\"input/TeX\"]," +
      "extensions: [\"tex2jax.js\",\"MathEvents.js\",\"MathMenu.js\",\"MathZoom.js\"]," +

      // No need to load early while we are adding messages.
      "delayStartupUntil: \"onload\",";

  if (!prefs.getBoolPref("showProcessing")) {
    config +=
      "showProcessingMessages: false," +
      "messageStyle: \"none\",";
  }

  if (prefs.getBoolPref("parseDollar")) {
    config +=
      "tex2jax: {" +
        "inlineMath: [['$','$'], [\"\\\\(\",\"\\\\)\"]], " +
        // Allow escaping of $ character
        "processEscapes: true" +
      "},";
  }

  config +=
      "TeX: {";

  if (prefs.getBoolPref("numberEquations"))
    config += "equationNumbers: {autoNumber: \"AMS\"},";

  config +=
        "extensions: [\"noErrors.js\",\"noUndefined.js\",\"AMSmath.js\",\"AMSsymbols.js\"]" +
      "}," +
      "\"HTML-CSS\": {" +
        "imageFont: null" +
      "}" +
    "});";

  if (aShouldScroll) {
    // Scroll to the end after the initial parse is complete.
    config += "MathJax.Hub.Register.StartupHook(\"End\", " +
      "function() {MathJax.Hub.Queue(window.scrollTo(0, window.scrollMaxY));});";
  }

  return config;
}

function initDocument(aDocument, aShouldScroll=true) {
  let container = aDocument.createElement("div");
  container.id = "mathjaxScriptContainer";

  let elt = aDocument.createElement("script");
  elt.type = "text/x-mathjax-config";
  elt.appendChild(aDocument.createTextNode(getConfiguration(aShouldScroll)));
  container.appendChild(elt);

  elt = aDocument.createElement("script");
  elt.src = "resource://mathjax/MathJax/MathJax.js";
  container.appendChild(elt);

  elt = aDocument.createElement("script");
  elt.src = "resource://mathjax/dynamicUpdate.js";
  container.appendChild(elt);

  let head = aDocument.querySelector("head");
  head.appendChild(container);

  // Hack: Bubbles context messages have a SVG filter that causes
  // strange blurring when used together with MathJax. Replace the
  // rule, if present, with opacity as a workaround.
  Array.forEach(aDocument.styleSheets, function(aStyleSheet) {
    if (aStyleSheet.cssRules) {
      for (let i = 0; i < aStyleSheet.cssRules.length; i++) {
        if (aStyleSheet.cssRules[i].selectorText &&
            aStyleSheet.cssRules[i].selectorText == ".bubble.context:not(:hover)") {
          aStyleSheet.deleteRule(i);
          aStyleSheet.insertRule(".bubble.context:not(:hover) {opacity:0.75}", i);
          break;
        }
      }
    }
  });
}

function resetDocument(aDocument) {
  let ibcontent = aDocument.getElementById("ibcontent");
  ibcontent.removeEventListener("DOMNodeInserted", ibcontent.handleNewNode);
  let container = aDocument.getElementById("mathjaxScriptContainer");
  container.parentNode.removeChild(container);
  let endOfConv = aDocument.getElementById("mathjax-addon-endofconv");
  endOfConv.parentNode.removeChild(endOfConv);
}

function observe(aSubject, aTopic, aData) {
  if (aTopic != "conversation-loaded")
    return;
  switch(aSubject.ownerDocument.documentElement.getAttribute("windowtype")) {
    case "Messenger:convs":
      initDocument(aSubject.contentDocument, true);
      break;
    case "Messenger:logs":
      initDocument(aSubject.contentDocument, false);
      break;
  }
}

function startup(aData, aReason)
{
  // Set default values for prefs.
  const kPrefDefaults = [
    ["showProcessing", true],
    ["parseDollar", true],
    ["numberEquations", false]
  ];
  kPrefDefaults.forEach(function([aKey, aValue]) {
    if (prefs.prefHasUserValue(aKey))
      return;
    switch(typeof aValue) {
      case "number":
        prefs.setIntPref(aKey, aValue);
        break;
      case "boolean":
        prefs.setBoolPref(aKey, aValue);
        break;
    }
  });

  // Restartless add-ons have no manifests: Add resource access to files by hand.
  // http://starkravingfinkle.org/blog/2011/01/restartless-add-ons-more-resources/
  let resource = Services.io.getProtocolHandler("resource")
                            .QueryInterface(Ci.nsIResProtocolHandler);
  let alias = Services.io.newFileURI(aData.installPath);
  if (!aData.installPath.isDirectory())
    alias = Services.io.newURI("jar:" + alias.spec + "!/", null, null);
  resource.setSubstitution("mathjax", alias);

  Conversations._conversations
               .forEach(function(aConvBinding) initDocument(aConvBinding.contentWindow.document));
  Services.obs.addObserver(observe, "conversation-loaded", false);
}

function shutdown(aData, aReason)
{
  Services.obs.removeObserver(observe, "conversation-loaded");
  Conversations._conversations
               .forEach(function(aConvBinding) resetDocument(aConvBinding.contentWindow.document));

  if (aReason == APP_SHUTDOWN)
    return;
  let resource = Services.io.getProtocolHandler("resource")
                            .QueryInterface(Ci.nsIResProtocolHandler);
  resource.setSubstitution("mathjax", null);
}

function install(aData, aReason) {}
function uninstall(aData, aReason) {}

